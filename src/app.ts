import * as path from 'path';
import { GraphQLServer } from 'graphql-yoga';
import cors from 'cors';

import { Mutation, Query, Question, Round, Session } from './resolvers';

const resolvers = {
    Mutation,
    Query,
    Question,
    Round,
    Session,
};

const app = new GraphQLServer({
    typeDefs: path.join(__dirname, '..', 'schema.graphql'),
    resolvers,
});

app.express.use(cors());

export default app;
