export enum RoundName {
    Simple = 'Simple',
    Double = 'Double',
    Triple = 'Triple',
    ViceVersa = 'ViceVersa',
}
