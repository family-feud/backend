import { Question } from './Question';

export interface Answer {
    id: number;
    name: string;
    questionId: Question['id'];
    priority: number;
}
