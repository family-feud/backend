export interface GraphQLResolver {
    [key: string]: (parent: any, args: any, context: any) => Promise<any>;
}
