import { RoundName } from '../enums';
import { Session } from './Session';
import { Question } from './Question';
import { Team } from './Team';

export interface Round {
    id: number;
    name: RoundName;
    bank: number;
    sessionCode: Session['code'];
    questionId: Question['id'];
    teamId: Team['id'];
    isStarted: boolean;
}
