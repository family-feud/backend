export interface Session {
    code: number;
    isHostConnected: boolean;
    isPlayerConnected: boolean;
    isSoundSwitchedOn: boolean;
}
