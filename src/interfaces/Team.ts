import { Session } from './Session';

export interface Team {
    id: string;
    sessionCode: Session['code'];
    name: string;
    points: number;
    errors: number;
    isCurrent: boolean;
}
