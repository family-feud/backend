import { connection } from '../db';
import { Answer, Question } from '../interfaces';

export class AnswersModel {
    public static getAnswersByQuestionId(
        questionId: Question['id']
    ): Promise<Answer[]> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM answers WHERE questionId = ? ORDER BY priority',
                [questionId],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results);
                }
            );
        });
    }
}
