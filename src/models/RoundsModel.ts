import { Round, Session } from '../interfaces';
import { connection } from '../db';
import { RoundName } from '../enums';

export class RoundsModel {
    public static getRoundBySessionCode(
        sessionCode: Session['code']
    ): Promise<Round> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM rounds WHERE sessionCode=?',
                [sessionCode],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results[0]);
                }
            );
        });
    }

    public static addRoundData({
        name = RoundName.Simple,
        bank = 0,
        isStarted = false,
        sessionCode,
        questionId,
        teamId,
    }: Omit<Round, 'id'>): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'INSERT INTO rounds (`name`, `bank`, `isStarted`, `sessionCode`, `questionId`, `teamId`) VALUE (?,?,?,?,?,?)',
                [name, bank, isStarted, sessionCode, questionId, teamId],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static changeRoundName(
        sessionCode: Session['code'],
        roundName: RoundName
    ): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE rounds SET `name`=? WHERE sessionCode=?',
                [roundName, sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static changeRoundBank(
        sessionCode: Session['code'],
        bank: Round['bank']
    ): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE rounds SET `bank`=? WHERE sessionCode=?',
                [bank, sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static changeRoundQuestionId(
        sessionCode: Session['code'],
        questionId: Round['questionId']
    ): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE rounds SET `questionId`=? WHERE sessionCode=?',
                [questionId, sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static changeRoundTeamId(
        sessionCode: Session['code'],
        teamId: Round['teamId']
    ): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE rounds SET `teamId`=? WHERE sessionCode=?',
                [teamId, sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static switchRoundIsStarted(
        sessionCode: Session['code']
    ): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE rounds SET `isStarted`= NOT `isStarted` WHERE sessionCode=?',
                [sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static changeRoundData({
        name = RoundName.Simple,
        bank = 0,
        isStarted = false,
        sessionCode,
        questionId,
        teamId,
    }: Omit<Round, 'id'>): Promise<Round['sessionCode']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE rounds SET `name`=?, `bank`=?, `isStarted`=?, `questionId`=?, `teamId`=? WHERE sessionCode=?',
                [name, bank, isStarted, questionId, teamId, sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static deleteRound(
        sessionCode: Session['code']
    ): Promise<Session['code'] | null> {
        return new Promise((resolve, reject) => {
            connection.query(
                'DELETE FROM rounds WHERE sessionCode=?',
                [sessionCode],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    if (results.affectedRows > 0) {
                        resolve(sessionCode);
                    } else {
                        resolve(null);
                    }
                }
            );
        });
    }
}
