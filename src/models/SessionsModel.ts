import { connection } from '../db';
import { Session } from '../interfaces';

export class SessionsModel {
    public static getSessionDataByCode(
        sessionCode: Session['code']
    ): Promise<Session> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM sessions WHERE code = ?',
                [sessionCode],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results[0]);
                }
            );
        });
    }

    public static addSessionData({
        code,
        isHostConnected = false,
        isPlayerConnected = false,
        isSoundSwitchedOn = false,
    }: Session): Promise<Session['code']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'INSERT INTO sessions (`code`, `isHostConnected`, `isPlayerConnected`, `isSoundSwitchedOn`) VALUE (?,?,?,?)',
                [code, isHostConnected, isPlayerConnected, isSoundSwitchedOn],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(code);
                }
            );
        });
    }

    public static switchIsHostConnected(
        sessionCode: Session['code']
    ): Promise<Session['code']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE sessions SET `isHostConnected`= NOT `isHostConnected` WHERE code=?',
                [sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static switchIsPlayerConnected(
        sessionCode: Session['code']
    ): Promise<Session['code']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE sessions SET `isPlayerConnected`= NOT `isPlayerConnected` WHERE code=?',
                [sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static switchIsSoundSwitchedOn(
        sessionCode: Session['code']
    ): Promise<Session['code']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE sessions SET `isSoundSwitchedOn`= NOT `isSoundSwitchedOn` WHERE code=?',
                [sessionCode],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(sessionCode);
                }
            );
        });
    }

    public static deleteSession(
        sessionCode: Session['code']
    ): Promise<Session['code'] | null> {
        return new Promise((resolve, reject) => {
            connection.query(
                'DELETE FROM sessions WHERE code=?',
                [sessionCode],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    if (results.affectedRows > 0) {
                        resolve(sessionCode);
                    } else {
                        resolve(null);
                    }
                }
            );
        });
    }
}
