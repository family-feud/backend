import { connection } from '../db';
import { Session, Team } from '../interfaces';

export class TeamsModel {
    public static getTeamsListBySessionCode(
        sessionCode: Session['code']
    ): Promise<Team[]> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM teams WHERE sessionCode = ?',
                [sessionCode],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results);
                }
            );
        });
    }

    public static getTeamById(id: Team['id']): Promise<Team> {
        return new Promise((resolve, reject) => {
            connection.query(
                'SELECT * FROM teams WHERE id = ?',
                [id],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results[0]);
                }
            );
        });
    }

    public static addTeamData({
        sessionCode,
        name,
        points = 0,
        errors = 0,
        isCurrent = false,
    }: Omit<Team, 'id'>): Promise<Team['id']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'INSERT INTO teams (`sessionCode`, `name`, `points`, `errors`, `isCurrent`) VALUE (?,?,?,?,?)',
                [sessionCode, name, points, errors, isCurrent],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(results.insertId);
                }
            );
        });
    }

    public static changeTeamPoints(
        teamId: Team['id'],
        points: Team['points']
    ): Promise<Team['id']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE teams SET `points`=? WHERE id=?',
                [points, teamId],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(teamId);
                }
            );
        });
    }

    public static changeTeamErrors(
        teamId: Team['id'],
        errors: Team['errors']
    ): Promise<Team['id']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE teams SET `errors`=? WHERE id=?',
                [errors, teamId],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(teamId);
                }
            );
        });
    }

    public static switchTeamIsCurrent(teamId: Team['id']): Promise<Team['id']> {
        return new Promise((resolve, reject) => {
            connection.query(
                'UPDATE teams SET `isCurrent`= NOT `isCurrent` WHERE id=?',
                [teamId],
                err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(teamId);
                }
            );
        });
    }

    public static deleteTeams(
        sessionCode: Session['code']
    ): Promise<Session['code'] | null> {
        return new Promise((resolve, reject) => {
            connection.query(
                'DELETE FROM teams WHERE sessionCode=?',
                [sessionCode],
                (err, results) => {
                    if (err) {
                        reject(err);
                    }

                    if (results.affectedRows > 0) {
                        resolve(sessionCode);
                    } else {
                        resolve(null);
                    }
                }
            );
        });
    }
}
