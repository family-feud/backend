import { GraphQLResolver } from '../interfaces';
import { RoundsModel, SessionsModel, TeamsModel } from '../models';

export const Mutation: GraphQLResolver = {
    addSessionData: async (_, { sessionData }) =>
        await SessionsModel.addSessionData(sessionData),
    switchIsHostConnected: async (_, { sessionCode }) =>
        await SessionsModel.switchIsHostConnected(sessionCode),
    switchIsPlayerConnected: async (_, { sessionCode }) =>
        await SessionsModel.switchIsPlayerConnected(sessionCode),
    switchIsSoundSwitchedOn: async (_, { sessionCode }) =>
        await SessionsModel.switchIsSoundSwitchedOn(sessionCode),
    deleteSession: async (_, { sessionCode }) =>
        await SessionsModel.deleteSession(sessionCode),

    addTeamData: async (_, { teamData }) =>
        await TeamsModel.addTeamData(teamData),
    changeTeamPoints: async (_, { teamId, points }) =>
        await TeamsModel.changeTeamPoints(teamId, points),
    changeTeamErrors: async (_, { teamId, errors }) =>
        await TeamsModel.changeTeamErrors(teamId, errors),
    switchTeamIsCurrent: async (_, { teamId }) =>
        await TeamsModel.switchTeamIsCurrent(teamId),
    deleteTeams: async (_, { sessionCode }) =>
        await TeamsModel.deleteTeams(sessionCode),

    addRoundData: async (_, { roundData }) =>
        await RoundsModel.addRoundData(roundData),
    changeRoundName: async (_, { sessionCode, roundName }) =>
        await RoundsModel.changeRoundName(sessionCode, roundName),
    changeRoundBank: async (_, { sessionCode, bank }) =>
        await RoundsModel.changeRoundBank(sessionCode, bank),
    changeRoundQuestionId: async (_, { sessionCode, questionId }) =>
        await RoundsModel.changeRoundQuestionId(sessionCode, questionId),
    changeRoundTeamId: async (_, { sessionCode, teamId }) =>
        await RoundsModel.changeRoundTeamId(sessionCode, teamId),
    switchRoundIsStarted: async (_, { sessionCode }) =>
        await RoundsModel.switchRoundIsStarted(sessionCode),
    changeRoundData: async (_, { roundData }) =>
        await RoundsModel.changeRoundData(roundData),
    deleteRound: async (_, { sessionCode }) =>
        await RoundsModel.deleteRound(sessionCode),
};
