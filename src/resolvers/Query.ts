import {
    AnswersModel,
    QuestionsModel,
    RoundsModel,
    SessionsModel,
    TeamsModel,
} from '../models';
import { GraphQLResolver } from '../interfaces';

export const Query: GraphQLResolver = {
    questionsList: async (_, { limit, offset }) =>
        await QuestionsModel.getList(limit, offset),
    questionById: async (_, { questionId }) =>
        await QuestionsModel.getById(questionId),
    randomQuestion: async () => await QuestionsModel.getRandom(),

    answersListByQuestionId: async (_, { questionId }) =>
        await AnswersModel.getAnswersByQuestionId(questionId),

    teamsListBySessionCode: async (_, { sessionCode }) =>
        await TeamsModel.getTeamsListBySessionCode(sessionCode),
    teamById: async (_, { teamId }) => await TeamsModel.getTeamById(teamId),

    sessionData: async (_, { sessionCode }) =>
        await SessionsModel.getSessionDataByCode(sessionCode),

    roundData: async (_, { sessionCode }) =>
        await RoundsModel.getRoundBySessionCode(sessionCode),
};
