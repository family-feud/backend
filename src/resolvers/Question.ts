import { GraphQLResolver } from '../interfaces';
import { AnswersModel } from '../models';

export const Question: GraphQLResolver = {
    answers: parent => AnswersModel.getAnswersByQuestionId(parent.id),
};
