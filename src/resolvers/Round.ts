import { GraphQLResolver } from '../interfaces';
import { AnswersModel, QuestionsModel, TeamsModel } from '../models';

export const Round: GraphQLResolver = {
    answers: ({ questionId }) =>
        AnswersModel.getAnswersByQuestionId(questionId),
    question: ({ questionId }) => QuestionsModel.getById(questionId),
    teams: ({ sessionCode }) =>
        TeamsModel.getTeamsListBySessionCode(sessionCode),
};
