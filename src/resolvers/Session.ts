import { GraphQLResolver } from '../interfaces';
import { TeamsModel } from '../models';

export const Session: GraphQLResolver = {
    teams: parent => TeamsModel.getTeamsListBySessionCode(parent.code),
};
