import app from './app';
import { ENVIRONMENT } from './util/secrets';

const PORT = process.env.PORT ?? 3000;
const options = {
    port: PORT,
    endpoint: '/graphql',
    playground: '/playground',
};

const server = app.start(options, () => {
    console.log(
        '  App is running at http://localhost:%d in %s mode',
        PORT,
        ENVIRONMENT
    );
    console.log('  Press CTRL-C to stop\n');
});

export default server;
