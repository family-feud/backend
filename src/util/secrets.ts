import logger from './logger';
import dotenv from 'dotenv';
import fs from 'fs';

if (fs.existsSync('.env')) {
    logger.debug('Using .env file to supply config environment variables');
    dotenv.config({ path: '.env' });
} else {
    logger.debug(
        'Using .env.100to1 file to supply config environment variables'
    );
    dotenv.config({ path: '.env.100to1' });
}
export const ENVIRONMENT = process.env.NODE_ENV;
const prod = ENVIRONMENT === 'production'; // Anything else is treated as 'dev'

export const MYSQL_URI = prod
    ? process.env['JAWSDB_URL']
    : process.env['MYSQL_URI_LOCAL'];

if (!MYSQL_URI) {
    if (prod) {
        logger.error(
            'No MySQL connection host. Set JAWSDB_URL environment variable.'
        );
    } else {
        logger.error(
            'No MySQL connection host. Set MYSQL_URI_LOCAL environment variable.'
        );
    }
    process.exit(1);
}
